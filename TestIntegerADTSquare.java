/**
*
* public class TestIntegerADTSquare
*
* test integer ADT, get a sum of square natural numbers
*
* ----- compile -----
*
* javac -g TestIntegerADTSquare
*
* ----- run -----
*
* java TestIntegerADTSquare 3
*
* ----- example output -----
*
* The number is 3.
* The sum of squares up to 3 is 14.
*
*/
public class TestIntegerADTSquare{
    public static void main(String args[]){
        int n;
        IntegerADT nadt,sumadt;

        if(args.length != 1){
            System.exit(1);
        }

        n=Integer.parseInt(args[0]);
        nadt=new IntegerADT(n);
        sumadt=nadt.sumsquare();
        System.out.println(String.format("The number is %d.",nadt.getSelf()));
        System.out.println(String.format("The sum of squares up to %d is %d.",nadt.getSelf(),sumadt.getSelf()));
    }
}
