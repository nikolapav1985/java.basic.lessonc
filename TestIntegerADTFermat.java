/**
*
* public class TestIntegerADTFermat
*
* test integer ADT to find if number is a prime
*
* ----- compile -----
*
* javac -g TestIntegerADTFermat
*
* ----- run -----
*
* java TestIntegerADTEFermat23
*
* ----- example output -----
*
* Is 23 a prime? true.
*
*/
public class TestIntegerADTFermat{
    public static void main(String args[]){
        int a,check;
        IntegerADT aadt,checkadt;
        boolean isprime;

        if(args.length != 1){
            System.exit(0);
        }

        a=Integer.parseInt(args[0]);
        aadt=new IntegerADT(a);
        check=3;
        checkadt=new IntegerADT(check);
        isprime=checkadt.isprimefermat(aadt);
        System.out.println(String.format("Is %d a prime? %s.",aadt.getSelf(),isprime));
    }
}
