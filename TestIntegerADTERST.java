/**
*
* public class TestIntegerADTERST
*
* test integer ADT to find if number is a prime
*
* ----- compile -----
*
* javac -g TestIntegerADTERST
*
* ----- run -----
*
* java TestIntegerADTERST 23
*
* ----- example output -----
*
* Is 23 a prime? true.
*
*/
public class TestIntegerADTERST{
    public static void main(String args[]){
        int a;
        IntegerADT aadt;
        boolean isprime;

        if(args.length != 1){
            System.exit(0);
        }

        a=Integer.parseInt(args[0]);
        aadt=new IntegerADT(a);
        isprime=aadt.isprimeerst();
        System.out.println(String.format("Is %d a prime? %s.",aadt.getSelf(),isprime));
    }
}
