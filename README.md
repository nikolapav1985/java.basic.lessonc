Basic lesson C
--------------

- IntegerADT.java (example Integer ADT, check comments for details)
- TestIntegerADT.java (example Integer ADT test, check comments for details)
- TestIntegerADTGCD.java (example Integer ADT gcd test, check comments for details)
- TestIntegerADTERST.java (example Integer ADT is prime test, check comments for details)
- TestIntegerADTOdd.java (example Integer ADT a sum of odd natural numbers, check comments for details)
- AddLoopFor.java (example for loop, check comments for details)
- AddLoopWhile.java (example while loop, check comments for details)
- AddLoopDoWhile.java (example do while loop, check comments for details)
- add.txt (example data for and while loop)
- add.dowhile.txt (example data do while loop)

Topic covered (loop for)
------------------------

- loop for(initial condition;run condition;after condition)...
- initial condition, done at loop start only once (e.g. initialize counters)
- run condition, if true continue loop, if false break loop
- after condition, do something after each iteration (e.g. increment counter)

Topic covered (loop for example)
--------------------------------

    for(i=0;i<n;i++){
        // i=0 initial counter value, set only once
        // i<n if true continue loop, if false (e.g. i>=n) break loop
        // i++ increment counter 
        // do stuff
    }

Topic covered (loop while)
--------------------------

- loop while(run condition)...
- run condition, if true continue loop, if false break loop

Topic covered (loop while example)
---------------------------------

    while(run condition){
        // if run condition true run iteration, if false break loop
        // do stuff
    }

Topic covered (loop do while)
-----------------------------

- loop do ... while(run condition)
- do iteration first
- check run condition next, if true continue loop, if false break loop

Topic covered (loop do while example)
-------------------------------------

    do{
        // do stuff
        // at least one iteration
        // after iteration check run condition
        // if run condition true, continue loop, if false break loop
    }while(run condition);

Document to print
-----------------

- basic.lessonc.tex (can be compiled to pdf and printed, e.g. pdflatex basic.lessonc.tex && pdflatex basic.lessonc.tex)

Practice (easy)
---------------

- read lines from file
- first line should be a number n
- all lines after (count n) are also numbers
- read lines and find highest number in a file
- can use any loop
- in example given next, read 3, this is number of data lines
- next read lines in turn
- and find highest number in the file (11 in this case)

Practice (example file)
-----------------------

    3
    9
    7
    11

Practice (again easy)
---------------------

- do the same but find least number in the file (7 in this case)

Practice (hard)
---------------

- add a solution for one TODO in IntegerADT

Practice (less hard)
--------------------

- add one test program for method (or methods) from IntegerADT
