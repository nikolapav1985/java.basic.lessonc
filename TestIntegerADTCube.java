/**
*
* public class TestIntegerADTCube
*
* test integer ADT, get a sum of cube natural numbers
*
* ----- compile -----
*
* javac -g TestIntegerADTCube
*
* ----- run -----
*
* java TestIntegerADTCube 3
*
* ----- example output -----
*
* The number is 3.
* The sum of cubes up to 3 is 36.
*
*/
public class TestIntegerADTCube{
    public static void main(String args[]){
        int n;
        IntegerADT nadt,sumadt;

        if(args.length != 1){
            System.exit(1);
        }

        n=Integer.parseInt(args[0]);
        nadt=new IntegerADT(n);
        sumadt=nadt.sumcube();
        System.out.println(String.format("The number is %d.",nadt.getSelf()));
        System.out.println(String.format("The sum of cubes up to %d is %d.",nadt.getSelf(),sumadt.getSelf()));
    }
}
