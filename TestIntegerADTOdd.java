/**
*
* public class TestIntegerADTOdd
*
* test integer ADT, get a sum of odd natural numbers
*
* ----- compile -----
*
* javac -g TestIntegerADTOdd
*
* ----- run -----
*
* java TestIntegerADTOdd 7
*
* ----- example output -----
*
* The number is 7.
* The sum of odds up to 7 is 49.
*
*/
public class TestIntegerADTOdd{
    public static void main(String args[]){
        int n;
        IntegerADT nadt,sumadt;

        if(args.length != 1){
            System.exit(1);
        }

        n=Integer.parseInt(args[0]);
        nadt=new IntegerADT(n);
        sumadt=nadt.sumodd();
        System.out.println(String.format("The number is %d.",nadt.getSelf()));
        System.out.println(String.format("The sum of odds up to %d is %d.",nadt.getSelf(),sumadt.getSelf()));
    }
}
