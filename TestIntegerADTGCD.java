/**
*
* public class TestIntegerADTGCD
*
* test integer ADT to find GDC greatest common denominator of couple of integers
*
* ----- compile -----
*
* javac -g TestIntegerADTGCD
*
* ----- run -----
*
* java TestIntegerADTGCD 35 15
*
* ----- example output -----
*
* Greatest common divisor of 35 and 15 is 5.
*
*/
public class TestIntegerADTGCD{
    public static void main(String args[]){
        int a,b;
        IntegerADT aadt,badt,gcdadt;

        if(args.length != 2){
            System.exit(0);
        }

        a=Integer.parseInt(args[0]);
        b=Integer.parseInt(args[1]);
        aadt=new IntegerADT(a);
        badt=new IntegerADT(b);
        gcdadt=aadt.gcd(badt);
        System.out.println(String.format("Greatest common divisor of %d and %d is %d.",aadt.getSelf(),badt.getSelf(),gcdadt.getSelf()));
    }
}
