import java.io.*;
import java.util.*;

/**
*
* public class AddLoopDoWhile
*
*/
public class AddLoopDoWhile{
    public static void main(String []args)throws FileNotFoundException
    {
        String filename;
        Scanner in;
        int a,b;

        if(args.length != 1){
            System.exit(1); // need one argument
        }

        filename=args[0];
        in=new Scanner(new File(filename));
        do{
            a=Integer.parseInt(in.nextLine());
            b=Integer.parseInt(in.nextLine());
            System.out.println(String.format("The sum of %d and %d is %d.",a,b,a+b));
        }while(in.hasNext());
    }
}
