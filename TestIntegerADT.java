/**
*
* public class TestIntegerADT
*
* test integer ADT, get a sum of natural numbers
*
* ----- compile -----
*
* javac -g TestIntegerADT
*
* ----- run -----
*
* java TestIntegerADT 4
*
* ----- example output -----
*
* The number is 4.
* The sum of naturals up to 4 is 10.
*
*/
public class TestIntegerADT{
    public static void main(String args[]){
        int n;
        IntegerADT nadt,sumadt;

        if(args.length != 1){
            System.exit(1);
        }

        n=Integer.parseInt(args[0]);
        nadt=new IntegerADT(n);
        sumadt=nadt.sumnatural();
        System.out.println(String.format("The number is %d.",nadt.getSelf()));
        System.out.println(String.format("The sum of naturals up to %d is %d.",nadt.getSelf(),sumadt.getSelf()));
    }
}
