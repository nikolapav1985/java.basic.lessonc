/**
*
* public class IntegerADT
*
* integer ADT abstract data type IMMUTABLE
*
*/
public class IntegerADT{
    public int self;
    public IntegerADT(int init){
        this.self=init;
    }
    public IntegerADT inc(){ // increment by one, producer
        return new IntegerADT(this.getSelf()+1);
    }
    public IntegerADT incn(IntegerADT other){ // increment by n, producer
        return new IntegerADT(this.getSelf()+other.getSelf());
    }
    public IntegerADT dec(){ // decrement by one, producer
        return new IntegerADT(this.getSelf()-1);
    }
    public IntegerADT decn(IntegerADT other){ // decrement by n, producer
        return new IntegerADT(this.getSelf()-other.getSelf());
    }
    public IntegerADT rem(IntegerADT other){ // remainder, this other, producer
        return new IntegerADT(this.getSelf()%other.getSelf());
    }
    public IntegerADT div(IntegerADT other){ // integer division, this other, producer
        return new IntegerADT(this.getSelf()/other.getSelf());
    }
    public IntegerADT mul(IntegerADT other){ // integer multiplication, this other, producer
        return new IntegerADT(this.getSelf()*other.getSelf());
    }
    public boolean less(IntegerADT other){ // check this less than other, observer
        return (this.getSelf() < other.getSelf());
    }
    public boolean more(IntegerADT other){ // check this more than other, observer
        return (this.getSelf() > other.getSelf());
    }
    public boolean eq(IntegerADT other){ // check this equal to other, observer
        return (this.getSelf() == other.getSelf());
    }
    public IntegerADT square(){ // square this number, producer
        return new IntegerADT(this.getSelf()*this.getSelf());
    }
    public boolean ismultipleof(IntegerADT other){ // check if this multiple of other, observer
        return ((this.getSelf()%other.getSelf()) == 0);
    }
    public IntegerADT modexp(IntegerADT exp, IntegerADT mod){ // a modular exponent, producer
        // this base >= 1
        // a ^ e = c mod n
        int i;
        IntegerADT product,tmp;
        if(this.getSelf() < 1){
            return new IntegerADT(-1);
        }
        product=new IntegerADT(1);
        for(i=0;i<exp.getSelf();i++){
            tmp=product.mul(this);
            product=null;
            product=tmp.rem(mod);
        }
        return product;
    }
    public IntegerADT modexpfast(IntegerADT exp, IntegerADT mod){ // a modular exponent fast, producer
        // reference
        // https://en.wikipedia.org/wiki/Modular_exponentiation
        IntegerADT result;
        IntegerADT base;
        IntegerADT exptmp;
        IntegerADT zero;
        IntegerADT tmp;
        IntegerADT too;
        if(this.getSelf() < 1){
            return new IntegerADT(-1);
        }
        result=new IntegerADT(1);
        base=new IntegerADT(this.getSelf());
        exptmp=new IntegerADT(exp.getSelf());
        zero=new IntegerADT(0);
        too=new IntegerADT(2);
        for(;!exptmp.eq(zero);){
            if(!exptmp.ismultipleof(too)){
                tmp=result.mul(base).rem(mod);
                result=null;
                result=tmp;
            }
            tmp=base.mul(base).rem(mod);
            base=null;
            base=tmp;
            tmp=exptmp.div(too);
            exptmp=null;
            exptmp=tmp;
        }
        return result;
    }
    public IntegerADT sumnatural(){ // a sum, this to 1, this >= 1, producer
        // a sum of natural numbers 1+2+3+...
        // sn=(1/2)n(n+1)
        int a,b;
        if(this.getSelf() < 1){
            return new IntegerADT(-1);
        }
        a=this.getSelf();
        b=a+1;
        if(a%2 == 0){
            a=a/2;
        }
        if(b%2 == 0){
            b=b/2;
        }
        return new IntegerADT(a*b);
    }
    public IntegerADT sumodd(){ // a sum, count this, this >= 1, producer
        // a sum of odd natural numbers 1+3+5+...
        // sn=n^2
        int n;
        if(this.getSelf() < 1){
            return new IntegerADT(-1);
        }
        n=this.getSelf();
        return new IntegerADT(n*n);
    }
    public IntegerADT sumsquare(){ // a sum, count this, this>=1, producer
        // a sum of squares 1^2 + 2^2 + 3^2 + ... 
        // sn=(1/2 1/3)n(n+1)(2n+1)
        int a,b,c;
        if(this.getSelf() < 1){
            return new IntegerADT(-1);
        }
        a=this.getSelf();
        b=this.getSelf();
        c=this.getSelf();
        b=b+1;
        c=2*c+1;
        if(a%2 == 0){
            a=a/2;
        }
        if(a%3 == 0){
            a=a/3;
        }
        if(b%2 == 0){
            b=b/2;
        }
        if(b%3 == 0){
            b=b/3;
        }
        if(c%2 == 0){
            c=c/2;
        }
        if(c%3 == 0){
            c=c/3;
        }
        return new IntegerADT(a*b*c);
    }
    public IntegerADT sumcube(){ // a sum, count this, this>=1, producer
        // a sum of cubes 1^3 + 2^3 +  3^3 + ...
        // sn=(1/4)n^2(n+1)^2 
        int a,b;
        if(this.getSelf() < 1){ // need natural numbers 1,2,3...
            return new IntegerADT(-1);
        }
        a=this.getSelf();
        b=this.getSelf();
        a=a*a;
        b=(b+1)*(b+1);
        if(a%4 == 0){
            a=a/4;
        }
        if(b%4 == 0){
            b=b/4;
        }
        return new IntegerADT(a*b);
    }
    // TODO print in number system e.g. this in binary octal hexd ...
    public IntegerADT gcd(IntegerADT other){ // gcd greatest common divisior this other, this > 0, other > 0, producer
        // greatest common divisor algorithm by euclid
        // a = b(q) + r
        // 5 = 3(1) + 1
        // 3 = 1(3) + 0
        IntegerADT zero;
        IntegerADT a;
        IntegerADT atmp;
        IntegerADT b;
        IntegerADT btmp;
        IntegerADT rem;
        IntegerADT remtmp;

        if(this.less(other)){ // need this > other
            return new IntegerADT(-1);
        }
        if(this.eq(other)){ // this equal to other
            return new IntegerADT(this.getSelf());
        }

        zero=new IntegerADT(0);
        rem=this.rem(other);
        a=new IntegerADT(this.getSelf());
        b=new IntegerADT(other.getSelf());
        for(;rem.eq(zero) == false;){
            atmp=new IntegerADT(b.getSelf()); // a=b
            a=null;
            a=atmp;
            btmp=new IntegerADT(rem.getSelf()); // b=rem
            b=null;
            b=btmp;
            remtmp=a.rem(b);
            rem=null;
            rem=remtmp;
        }

        return b;
    }
    // TODO fermat number
    // TODO mersenne number
    public boolean isprimefermat(IntegerADT p){ // check is prime, apply fermat test, observer
        // a^p = a mod p (p should be a prime this)
        // apply for natural numbers 1,2,3...
        IntegerADT check=new IntegerADT(this.getSelf()); // a small prime used to run the test e.g. a=3
        IntegerADT checkagain=this.modexpfast(p,p);
        return check.eq(checkagain);
    }
    public boolean isprimeerst(){ // check this is prime, use sieve by erasthotenes, observer
        IntegerADT prime;
        IntegerADT primesq;
        IntegerADT primetmp;
        boolean check;

        prime=new IntegerADT(2); // first prime, also first divisor to check
        check=false;        

        for(;true;){
            primesq=prime.square();
            if(primesq.more(this)){ // square more than this, count done
                check=true;
                break;
            }
            primesq=null;
            if(this.ismultipleof(prime)){
                check=false;
                break;
            }
            primetmp=prime.inc();
            prime=null;
            prime=primetmp;
        }

        return check;
    }
    public int getSelf(){ // self, observer
        return this.self;
    }
}
