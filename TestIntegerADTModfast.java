/**
*
* public class TestIntegerADTModfast
*
* test integer ADT, get a sum of cube natural numbers
*
* ----- compile -----
*
* javac -g TestIntegerADTModfast
*
* ----- run -----
*
* java TestIntegerADTModfast 3
*
* ----- example output -----
*
* The modular exponent 5 ^ 117 mod 19 is 1.
*
*/
public class TestIntegerADTModfast{
    public static void main(String args[]){
        int base,exp,mod;
        IntegerADT baseadt,expadt,modadt,doneadt;

        if(args.length != 3){
            System.exit(1);
        }

        base=Integer.parseInt(args[0]);
        exp=Integer.parseInt(args[1]);
        mod=Integer.parseInt(args[2]);
        baseadt=new IntegerADT(base);
        expadt=new IntegerADT(exp);
        modadt=new IntegerADT(mod);
        doneadt=baseadt.modexpfast(expadt,modadt);
        System.out.println(String.format("The modular exponent %d ^ %d mod %d is %d.",baseadt.getSelf(),expadt.getSelf(),modadt.getSelf(),doneadt.getSelf()));
    }
}
