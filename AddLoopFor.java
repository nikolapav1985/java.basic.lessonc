import java.io.*;
import java.util.*;

/**
*
* public class AddLoopFor
*
*/
public class AddLoopFor{
    public static void main(String []args)throws FileNotFoundException
    {
        String filename;
        Scanner in;
        int a,b,count,i;

        if(args.length != 1){
            System.exit(1); // need one argument
        }

        filename=args[0];
        in=new Scanner(new File(filename));
        count=Integer.parseInt(in.nextLine());
        for(i=0;i<count;i++){
            a=Integer.parseInt(in.nextLine());
            b=Integer.parseInt(in.nextLine());
            System.out.println(String.format("The sum of %d and %d is %d.",a,b,a+b));
        }
    }
}
